Rails.application.routes.draw do
  get 'sessions/new'

  resources :users

  match '/faq',        to: 'static_pages#faq', :via => [:get]
  get   '/rules'       =>  'static_pages#rules'  
  root                 to: 'static_pages#home'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  get    'new'  => 'boards#new'
  post    'boards'  => 'boards#create'

  resources :account_activations, only: [:edit]
  get 'boards' => "boards#index"
  delete 'nuke/:board_address' => 'boards#destroy', as: "nuke"
  post   'create_ip_status' => 'ip_statuses#create', as: 'create_ip_status'
  delete 'destroy_msg' => 'messages#destroy', as: "destroy_msg"
  #post 'messages' => 'messages#create_topic'
  #post 'replies'  => 'messages#create_reply'
  get 'topics' => 'topics#index'  
  
  
  resources :boards, param: :address, constraints: {address: /[a-z0-9]+/}, only: [:show], path: '/', as: "board" do
    post 'messages' => 'messages#create'
    post 'assign'   => 'user_board_relationships#create'
    delete 'unassign/:id' => 'user_board_relationships#destroy', as: 'unassign'
    #delete 'destroy_ip_status/:id' => 'ip_statuses#destroy', as: 'destroy_ip_status'
    get 'edit' => 'boards#edit', as: "edit"                #board_edit_path
    get 'mod'  => 'boards#mod', as:  "mod"
    delete 'mod' => 'ip_statuses#destroy', as: 'destroy_ip_status'
    patch 'update' => 'boards#update', as: 'update'
    get 'users' => 'boards#users'
    resources :topics, only: [:show], path: '/', as: "topic"
    #I'm sure this is nasty way to do stuff, but at least it lets me make validation errors 'friendly'
    post '' => 'messages#create', as: ''       #for smooth posts=>topic #board_path
    post ':id' => 'messages#create', as: 'msg' #for smooth replies      #board_msg_path
    #/nasties
  end

  unless Rails.application.config.consider_all_requests_local
    match '*not_found', to: 'static_pages#error_404', :via => [:get]
  end
  

  #this should be below everything else!
#begin
#  resources :boards, constraints: {address: /[a-z0-9]+/}, param: :address, only: [:show], path: :'/' 
#end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
