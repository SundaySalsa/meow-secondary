Static
  errors
  413 request entity too large
Users
#  create
#  new
#  edit
#  index
#  show
#  about
#  hide email
#  fix breadcrumb
#  update

#  valid user regex
  restore password
#  log in with name

Boards
#  index
#  show
#  edit
#  create
#  place link on visible spot
#  new
#  destroy
  forbid destruction for boards with more than 50 topics and test that
  wordfilter
  move owner assignement from controller into model
    test it

  custom styles
  custom flags
  custom favicons
  custom ids
  custom watermarks
  board aliases
  custom domains

Topics
#  new
#  create
#  show
#  autorefresh

Messages
#  create
#  destroy
  edit?
#  add random ids
#  render message form instead of redirecting
  rewrite tests accordingly
  alternate spoiler syntax [Visible](#s "invisible")

  content slug
    #http://viralpatel.net/blogs/dynamically-shortened-text-show-more-link-jquery/
    #This is good implementation, but in our case it should be based on height, like this:
    #$('#post_id').height();
    #I don't know what to do with markup, though
    #http://css-tricks.com/almanac/properties/w/word-break/

Files
#  create
  destroy
  spoiler
  #fix disappearing files bug
  spoofing test
  form clearing button for files

IpStatuses
#  create
#  index
#  destroy
  test it
  remove duplicates
  add warnings
  add js-based in-board bans
  
ModLog
  ???

#login with username
#post quoting
#mod assignment
write integration tests for files
#write faq
#fix ips/ip_digests in messages_controller
#fix user board owner relationships
remove repeating code from shared/_op, shared/_reply
autoload messages
Обкатать установку вебсокетов на продакшене

Javascript
#  id coloring
  fix onload for turbolinks
#  image expanding
#  quoting

fix secrets
HTML & CSS
  remove bootstrap or add alternate styles
  fix glyphs
  
Haboda
