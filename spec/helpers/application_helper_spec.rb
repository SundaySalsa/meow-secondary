require "spec_helper"

describe ApplicationHelper do
  describe "#markup" do
    examples = {
      "[b]bold[/b]"             => "<b>bold</b>",
      "[i]italic[/i]"           => "<em>italic</em>",
      "[b]not[/b][b]greedy[/b]" => "<b>not</b><b>greedy</b>",
      "<<html safety>"       => "&lt;&lt;html safety&gt;",
      "[spoiler]secret[/spoiler]" =>
                "<span class='spoiler'>secret</span>",
      "[u]underline[/u]"        => "<u>underline</u>",
      "[s]strike[/s]"           => "<s>strike</s>",
      "http://google.com"       => '<a rel="nofollow" href="http://google.com">http://google.com</a>',
      #Do we need more link tests?

      #single-stringers
      "**bold**"                => "<b>bold</b>",
      "*italic*"                => "<em>italic</em>",
      "%%secret%%"             =>
                "<span class='spoiler'>secret</span>",


      #quotes
    }

    examples.each do |raw, cooked|
      it "should turn #{raw} into #{cooked}" do
        markup(raw).should eq cooked
      end
    end

#    it "returns true" do
#      helper.markup('').should be_true
#    end
  end
end

