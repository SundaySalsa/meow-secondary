require 'spec_helper'

describe "StaticPages" do

  describe "Home page" do
    it "should have the content 'Meowta' or something" do
      visit root_path
      page.should have_content(I18n.t "meowta")
    end
  end

  describe "FAQ page" do
    #should exist
    #should have markup
    it "should have valid breadcrumbs" do
      visit faq_path
      page.should have_selector('li', text: (I18n.t "faq"))
      #it should be a link
    end
  end
end
