class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string :title
      t.string :address
      t.string :description

      t.timestamps
    end
  end
end
