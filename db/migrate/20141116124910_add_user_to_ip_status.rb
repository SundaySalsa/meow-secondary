class AddUserToIpStatus < ActiveRecord::Migration
  def change
    add_reference :ip_statuses, :user, index: true
  end
end
