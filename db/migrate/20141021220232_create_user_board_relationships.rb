class CreateUserBoardRelationships < ActiveRecord::Migration
  def change
    create_table :user_board_relationships do |t|
      t.string :role
      t.integer :user_id
      t.integer :board_id

      t.timestamps
    end
    add_index :user_board_relationships, :user_id
    add_index :user_board_relationships, :board_id
    #add_index :user_board_relationships, [:user_id, :board_id], unique: true
  end
end
