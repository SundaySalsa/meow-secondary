class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.string :type
      t.references :message

      t.timestamps
    end
    add_attachment :uploads, :file
  end
end
