class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :content, default: ""
      t.string :ip_digest
      t.string :public_name, default: "Porcupine"
      t.references :topic, index: true
      t.references :board, index: false

      t.timestamps
    end
  add_index :messages, [:topic_id, :updated_at]
  end
end
