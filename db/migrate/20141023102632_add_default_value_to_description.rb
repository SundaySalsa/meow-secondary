class AddDefaultValueToDescription < ActiveRecord::Migration
  def change
    change_column :boards, :description, :string, :default => ""
  end
end
