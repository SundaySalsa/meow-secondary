class CreateIpStatuses < ActiveRecord::Migration
  def change
    create_table :ip_statuses do |t|
      t.string :ip_digest
      t.references :board, index: true
      t.string :status
      t.string :reason
      t.datetime :expires_at

      t.timestamps
    end
    add_index :ip_statuses, [:board_id, :created_at]
    add_index :ip_statuses, [:board_id, :expires_at]
  end
end
