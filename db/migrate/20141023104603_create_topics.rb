class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.boolean :sticky, default: false
      t.boolean :open, default: true
      t.references :board, index: true

      t.timestamps
    end
    add_index :topics, [:board_id, :sticky, :updated_at]
  end
end
