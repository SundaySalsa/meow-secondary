class AddFileFingerprintToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :file_fingerprint, :string
    add_column :uploads, :board_id, :integer
    add_index :uploads, [:board_id, :file_fingerprint]
  end
end
