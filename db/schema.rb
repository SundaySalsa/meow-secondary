# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141116124910) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boards", force: true do |t|
    t.string   "title"
    t.string   "address"
    t.string   "description", default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ip_statuses", force: true do |t|
    t.string   "ip_digest"
    t.integer  "board_id"
    t.string   "status"
    t.string   "reason"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "ip_statuses", ["board_id", "created_at"], name: "index_ip_statuses_on_board_id_and_created_at", using: :btree
  add_index "ip_statuses", ["board_id", "expires_at"], name: "index_ip_statuses_on_board_id_and_expires_at", using: :btree
  add_index "ip_statuses", ["board_id"], name: "index_ip_statuses_on_board_id", using: :btree
  add_index "ip_statuses", ["user_id"], name: "index_ip_statuses_on_user_id", using: :btree

  create_table "messages", force: true do |t|
    t.text     "content",           default: ""
    t.string   "ip_digest"
    t.string   "public_name",       default: "Porcupine"
    t.integer  "topic_id"
    t.integer  "board_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "processed_content"
  end

  add_index "messages", ["topic_id", "updated_at"], name: "index_messages_on_topic_id_and_updated_at", using: :btree
  add_index "messages", ["topic_id"], name: "index_messages_on_topic_id", using: :btree

  create_table "simple_captcha_data", force: true do |t|
    t.string   "key",        limit: 40
    t.string   "value",      limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_captcha_data", ["key"], name: "idx_key", using: :btree

  create_table "topics", force: true do |t|
    t.boolean  "sticky",     default: false
    t.boolean  "open",       default: true
    t.integer  "board_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "topics", ["board_id", "sticky", "updated_at"], name: "index_topics_on_board_id_and_sticky_and_updated_at", using: :btree
  add_index "topics", ["board_id"], name: "index_topics_on_board_id", using: :btree

  create_table "uploads", force: true do |t|
    t.string   "type"
    t.integer  "message_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.string   "file_fingerprint"
    t.integer  "board_id"
  end

  add_index "uploads", ["board_id", "file_fingerprint"], name: "index_uploads_on_board_id_and_file_fingerprint", using: :btree

  create_table "user_board_relationships", force: true do |t|
    t.string   "role"
    t.integer  "user_id"
    t.integer  "board_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_board_relationships", ["board_id"], name: "index_user_board_relationships_on_board_id", using: :btree
#  add_index "user_board_relationships", ["user_id", "board_id"], name: "index_user_board_relationships_on_user_id_and_board_id", unique: true, using: :btree
  add_index "user_board_relationships", ["user_id"], name: "index_user_board_relationships_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "admin",             default: false
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_digest"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
