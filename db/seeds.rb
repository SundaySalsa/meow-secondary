# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create!(name:  "spamguy",
             email: "vasya@mail.ru",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.first_name + '.' + Faker::Name.last_name
  email = "vasya-#{n+1}@mail.ru"
  password = "foobar"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end


97.times do |n|
  title = Faker::Company.name + "-#{n}"
  address = Faker::Hacker.abbreviation + "#{n}"
  description = Faker::Commerce.department
  @user = User.find((n+22)/22)
  @user.boards.create!(title: title,
                              address: address,
                              description: description)
end

3.times do |board|
  @board = Board.all[-board]
  22.times do |n|
    msg = @board.messages.create!(content: Faker::Lorem.paragraph(4),
                                  ip: [rand(255),rand(255),rand(255),rand(255)].join('.'))
    rand(10).times do |x|
      msg.topic.messages.create!(content: Faker::Lorem.paragraph(4),
                                 ip: [rand(255),rand(255),rand(255),rand(255)].join('.'))
    end
  end
end
