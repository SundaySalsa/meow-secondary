require 'test_helper'

class BoardsShowTest < ActionDispatch::IntegrationTest

  def setup
    @board     = boards(:orange)
    @admin     = users(:michael)
    # I don't know why, but this doesn't work:
    @post   =    messages(:thread)
  end

  test "Boards should have working edit links" do
    log_in_as(@admin)
    get board_path(@board)
    assert_select 'a', text: (I18n.t :edit_board)
    get board_edit_path(@board)
    assert_template 'boards/edit'
  end

  test "Threads should have working edit links" do
    log_in_as(@admin)
    #skip("I have no idea why I have to define it here")
    a = @board.messages.create!(content: "bdsfds")
    get board_topic_path(@board, a.topic)
    assert_select 'a', text: (I18n.t :edit_board)
  end
end
