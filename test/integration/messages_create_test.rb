require 'test_helper'

# See topics_create_test.rb for creating messages from board scope

class MessagesCreateTest < ActionDispatch::IntegrationTest

  def setup
    @board = boards(:orange)
    #@op = messages(:thread)
    @op = @board.messages.create!(content: "fadsfs")
    @topic = @op.topic
  end

  test "valid message data" do
    get board_topic_path(@topic.board.address, @topic)
    tpc_count = Topic.count
    content = Faker::Lorem.paragraph(4) 
    assert_difference 'Message.count', 1 do
      # post_via_redirect replies_path...
      # Not sure how to test using correct form. And I don't want to hardcode form names. It'll crash, but it will give a weird message
      post_via_redirect board_messages_path(@board), message: { content: content,
                                                                topic_id: @topic.id } #Passed through hidden field 
    end
    assert_template 'topics/show'
    assert Topic.count == tpc_count
  end

end
