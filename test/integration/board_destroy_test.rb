require 'test_helper'

class BoardDestroyTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @owner = users(:archer)
    @board = @owner.boards.create!(title: "dsafsd", address: "dsafsfgdf", description: 'fdsbnf')
    @user = users(:mallory)
  end

  test "admin should be able to destroy boards" do
    #assert_select 'a', text: (I18n.t :destroy_board)
    log_in_as(@admin)
    assert_difference 'Board.count', -1 do
      delete_via_redirect nuke_path(@board.address)
    end
    assert_template 'boards/index'
  end

  test "anonymous & regular users should not be able to destroy boards" do
    get boards_path
    assert_select 'a', {count: 0, text: (I18n.t :destroy_board)}, "Privelegeless users should not see deletion links"
    assert_difference 'Board.count', 0 do
      delete_via_redirect nuke_path(@board.address)
    end
    log_in_as(@user)
    assert_difference 'Board.count', 0 do
      delete_via_redirect nuke_path(@board.address)
    end
  end
end
