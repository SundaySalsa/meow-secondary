require 'test_helper'

class BoardCreateTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  # Don't repeat yourself, they said
  def create_attempt(title, address, description, difference)
    get new_path
    assert_difference 'Board.count', difference do
      post_via_redirect boards_path, board: { title:  title,
                                            address: address,
                                            description:              description }
    end
  end

  test "with valid information" do
    log_in_as(@user)
    create_attempt("Anime", "anumu", "Blah blah \n blah", 1)
    assert_template 'boards/show'
  end

  test "with invalid information" do
    log_in_as(@user)
    create_attempt("", "anumu", "Blah blah \n blah", 0)
    assert_template 'boards/new'
  end

  test "as non-user" do
    create_attempt("", "anumu", "Blah blah \n blah", 0)
    assert_template 'sessions/new'
  end

  test "illegal names" do
    skip "I'll do it late"
  end
end
