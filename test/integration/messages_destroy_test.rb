require 'test_helper'

class MessagesDestroyTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @board = boards(:orange)
    @op = messages(:thread)
    @user = users(:mallory)
    @mod = users(:archer)
    @board.assign_user(@mod)
  end

  test "Anons and regular users shouldn't see mod page" do
    get board_mod_path(@board)
    assert_redirected_to root_path
    log_in_as(@user)
    get board_mod_path(@board)
    assert_redirected_to root_path
  end
  
  test "Delete message as admin" do
    log_in_as(@admin)
    get board_mod_path(@board)
    assert_template "boards/mod"
    assert_difference 'Message.count', -1 do
      delete_via_redirect destroy_msg_path, id: @op.id
    end
  end
  
  test "Delete message as mod" do    
    log_in_as(@mod)
    get board_mod_path(@board)
    assert_template "boards/mod"
    assert_difference 'Message.count', -1 do
      delete_via_redirect destroy_msg_path, id: @op.id
    end
  end  
  
  test "Delete message as user" do
    log_in_as(@user)
    get board_mod_path(@board)
    assert_redirected_to root_path
    assert_difference 'Message.count', 0 do
      delete_via_redirect destroy_msg_path, id: @op.id
    end       
  end    

  test "Cross-board message deletion should not work" do
    @msg = messages(:tomato_thread)
    log_in_as(@mod)
    assert_difference 'Message.count', 0 do
      delete_via_redirect destroy_msg_path, id: @msg.id
    end       
  end
  #No id
  #Nested deletion
  #Reply deletion
  #Cross-board deletion by mods
end
