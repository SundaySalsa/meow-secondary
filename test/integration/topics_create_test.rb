require 'test_helper'

# Technically, it's message create test
# Or Thread creation in imageboard context
# Message is born from Board, and creates associated Topic
# On contrary, reply creation (described in messages_create_test.rb) attaches new message to existing Topic
# I'm sorry for this mess

class TopicsCreateTest < ActionDispatch::IntegrationTest

  def setup
    @board = boards(:orange)
  end

  test "valid message data" do
    get board_path(@board)
    msg_count = Message.count
    content = Faker::Lorem.paragraph(4) 
    assert_difference 'Topic.count', 1 do
      post_via_redirect board_messages_path(@board), message: { content: content}
    end
    assert_template 'topics/show'
    assert Message.count == (msg_count + 1)
  end

  test "invalid message data" do
    # It should test whether I use rendering instead of redirecting
    # Rendering seems to keep data entered by users, while redirect destroys it
    get board_path(@board)
    msg_count = Message.count
    content = "" #Empty 
    assert_difference 'Topic.count', 0 do
      post_via_redirect board_messages_path(@board), message: { content: content}
    end
    assert_template 'messages/new'    #hardcode, not cool
    assert_select 'div.has-error'     # Pops out in bootstrap-forms whenever error strikes, feel free to change it
    assert Message.count == (msg_count)
  end

end
