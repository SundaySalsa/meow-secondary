require 'test_helper'

class BoardUsersTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @board = boards(:cat_videos)
    @owner = users(:lana)
    @board.assign_user(@owner, 'owner')
    @mod = users(:archer)   
    @board.assign_user(@mod, 'mod')
    @user = users(:mallory)
  end

  test "Board users page should list users properly" do
    get board_users_path(@board)
    assert_select 'a', @mod.name
    assert_select 'a', @owner.name
  end

  test "Owner should be able to assign and unassign users" do
    log_in_as(@owner)
    get board_users_path(@board)
    assert_select 'legend', true, "Owner should see user assignment form"
    assert_difference '@board.users.count', 1 do
      post_via_redirect board_assign_path(@board), name: @user.name
    end
    assert_difference '@board.users.count', -1 do
      delete_via_redirect board_unassign_path(@board, @user)
    end
  end  
  
  test "Same user can't be assigned multiple times" do
=begin
    log_in_as(@owner)
    assert_difference '@board.users.count', 1 do
      post_via_redirect board_assign_path(@board), name: @user.name
      post_via_redirect board_assign_path(@board), name: @user.name
    end
=end
    skip('todo')    
  end
  
  test "Mods should not be able to assign or unassign users" do
    log_in_as(@mod)
    get board_users_path(@board)
    assert_select 'legend', 0, "Mods should not see the form"
    assert_difference '@board.users.count', 0 do
      post_via_redirect board_assign_path(@board), name: @user.name
    end
  end
  
end
