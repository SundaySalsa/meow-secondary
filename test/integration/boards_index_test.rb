require 'test_helper'

class BoardsIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin     = users(:michael)
    @board_owner = users(:archer)
    @board = @board_owner.boards.create!(title: "ffffff", address: "fffffdsf")
  end

  test "showing list of boards with pagination" do
    get boards_path
    #assert_select 'div.pagination'
  end

  test "as board admin" do
    skip("Edit is not implemented yet")
  end

  test "edit links" do
    skip("Edit is not implemented yet")
  end

  test "order of boards" do
    skip("Messages are not implemented yet")
  end
end
