require 'test_helper'

class MessagesShowTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @board = boards(:orange)
    @op = messages(:thread)
    @user = users(:mallory)
    @mod = users(:archer)
    @board.assign_user(@mod)
  end

  test "Mods and admins should be able to see user ids" do
    get board_path(@board)
    assert_select 'li', @op_digest, 0
    log_in_as @mod
    get board_path(@board)
    assert_select 'li', @op_digest
    end
end
