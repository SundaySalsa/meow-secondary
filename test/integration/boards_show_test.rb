require 'test_helper'

class BoardsShowTest < ActionDispatch::IntegrationTest

  def setup
    @board     = boards(:orange)
    @admin     = users(:michael)
  end

  test "Should have title and description" do
    get board_path(@board)
    assert_select 'h1', @board.title
  end

  test "Boards should have short name" do
    get board_path(@board)
    assert_routing "/orange", :action => "show", :controller => "boards", :address => "orange"
  end
end
