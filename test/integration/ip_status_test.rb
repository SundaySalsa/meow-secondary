require 'test_helper'

class BoardDestroyTest < ActionDispatch::IntegrationTest
include ApplicationHelper

  def setup
    @admin = users(:michael)
    @board = boards(:orange)
    @op = messages(:thread)
    @user = users(:mallory)
    @mod = users(:archer)
    @board.assign_user(@mod)
    @ip_digest = create_ip_digest('128.0.0.9')
  end

  test "Mod should be able to create bans" do
    log_in_as(@mod)
    get board_mod_path(@board)
    assert_template 'boards/mod' 
    assert_difference 'IpStatus.count', 1 do
    post_via_redirect create_ip_status_path, ip_status: { ip_digest: @ip_digest,
                                                          reason: 'Test',
                                                          days_left: '5',
                                                          board_address: @board.address}
    end
    assert_equal IpStatus.last.reason, 'Test'
    assert IpStatus.last.expires_at > 3.days.from_now    
  end
  
  test "Banned users should not be able to post" do
    #Banning 127.0.0.1. I have no idea how to assign other ip to requests in tests
    @board.ip_statuses.create!(ip_digest: create_ip_digest('127.0.0.1'))
    assert_difference 'Message.count', 0 do
      post_via_redirect board_messages_path(@board), message: { content: "HWOOOOOO" }
    end  
  end
  
  test "Legit users should be able to destroy bans" do
    skip "todo"
  end
  
  test "Ban lists should be rendered properly" do
    skip "todo"
  end
  
  test "Mods should not be able to ban users on other boards" do
    skip "todo"
  end
end
