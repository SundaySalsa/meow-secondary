# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  sticky     :boolean          default(FALSE)
#  open       :boolean          default(TRUE)
#  board_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'test_helper'

class BoardsShowTest < ActionDispatch::IntegrationTest

# I'm not very sure how stickies should behave around each other, so that is not tested

  test "order should show stickies first" do
    assert_equal Topic.first, topics(:sticky_new)
  end

  #test "oldest threads should be below" do
  #  assert_equal Topic.last, messages(:old_thread).topic
  #end
  #freshest/oldest first/last
  #new posts should bump threads properly

end
