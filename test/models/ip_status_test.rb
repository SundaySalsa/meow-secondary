# == Schema Information
#
# Table name: ip_statuses
#
#  id         :integer          not null, primary key
#  ip_digest  :string(255)
#  board_id   :integer
#  status     :string(255)
#  reason     :string(255)
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#

require 'test_helper'

class IpStatusTest < ActionDispatch::IntegrationTest

  def setup
    @board = boards(:orange)                 
    @ip_status = IpStatus.create!(ip: '127.0.0.1',
                                  board: @board,
                                  reason: "Прост)))",
                                  expires_at: 1.day.from_now)
  end

  test "Ip status should be valid" do
    assert @ip_status.valid?
  end

  test "Board should know its IpStatuses" do
    assert_equal @board.ip_statuses.last, @ip_status
    assert_equal @ip_status.board, @board
  end

  test "Default values" do
    assert_equal @ip_status.status, "ban"
  end

  test "Invalid ip" do
    @wrong_is = IpStatus.create(ip: '0',
                                  board: @board,
                                  reason: "Прост)))",
                                  expires_at: 1.day.from_now)
    skip("For some reason it thinks nil is not empty")
    #assert_not @wrong_is.valid?
  end
end
