# == Schema Information
#
# Table name: messages
#
#  id          :integer          not null, primary key
#  content     :text             default("")
#  ip_digest   :string(255)
#  public_name :string(255)      default("Porcupine")
#  topic_id    :integer
#  board_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'test_helper'

class MessageTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    # @user.build doesn't work as intended. Use "create" instead
    # https://github.com/rails/rails/issues/6161
    @board = @user.boards.create!(title: "Lorem", address: "Ipsum", description: "Literally none")
    @post = @board.messages.create!(content: Faker::Hacker.say_something_smart, 
                                    ip_digest: "342dffdsf",
                                    public_name: "Porcupine")       
    #@topic = @post.topic, yesh-yesh, it's sensitive and tested below                    
  end

  test "Message should be valid" do
    assert @post.valid?
  end

  test "Empty messages should not be valid" do
    @invalid_post = @board.messages.create(content: "", 
                                    ip_digest: "342dffdsf",
                                    public_name: "Porcupine") 
    assert_not @invalid_post.valid?
  end

  test "Message should create topic, if created from board" do
    assert_not @post.topic.nil?
  end

  test "Replies should be assigned to other topic" do
    reply = @post.topic.messages.create!(content: "reply", 
                                 ip_digest: "342dffdsf",
                                 public_name: "Porcupine")
    assert reply.topic == @post.topic 
  end
end
