# == Schema Information
#
# Table name: uploads
#
#  id                :integer          not null, primary key
#  type              :string(255)
#  message_id        :integer
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  file_fingerprint  :string(255)
#  board_id          :integer
#

require 'test_helper'

class UploadsShowTest < ActionDispatch::IntegrationTest
  def setup
    @message = messages(:thread)
    @upload = @message.uploads.create!(file: File.new("test/fixtures/paperclip/1.jpg"))
  end

  test "images should be valid" do
    assert @upload.valid?
  end

  test "accessing attributes" do
    assert_not_nil @message.uploads[0].file.url
  end
end
