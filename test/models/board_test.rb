# == Schema Information
#
# Table name: boards
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  address     :string(255)
#  description :string(255)      default("")
#  created_at  :datetime
#  updated_at  :datetime
#

require 'test_helper'

class BoardTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    # @user.build doesn't work as intended. Use "create" instead
    # https://github.com/rails/rails/issues/6161
    @board = @user.boards.create(title: "Lorem", address: "Ipsum", description: "Literally none")
  end

  test "board should be valid" do
    assert @board.valid?
  end

  test "board owner should be present" do
    assert_equal @board.owner, @user
  end
end
