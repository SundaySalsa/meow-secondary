require 'test_helper'

class UserBoardRelationshipsControllerTest < ActionController::TestCase

  def setup
    @board = boards(:orange)
    @user = users(:mallory)
    @mod = users(:archer)
  end

  test "Board created from user should have owner" do
    @board1 = @user.boards.create!(title: "dsfsfs", address: 'dsfsf')
    assert_equal @board1.owner, @user
    assert_equal @board1.user_board_relationships[0].role, 'owner'
  end
  
  test "Users assigned to board should be mods" do
    @board1 = @user.boards.create!(title: "dsfsfs", address: 'dsfsf')
    @board1.assign_user(@mod)
    assert_equal @board1.user_board_relationships.last.role, 'mod'
  end
end
