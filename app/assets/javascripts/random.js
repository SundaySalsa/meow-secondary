//Random-flavored helper functions

//Seeded random, aka Ruby's srand(x)
var seed = 1;
function srand() {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}

//Gets string, returns pseudorandom number depending on its content
function string_to_seed(str) {
  if (str.length > 1) {
    num = str.trim()[0].charCodeAt(0);}
  else {
    num = 0;
  }
  return num
}

//Paints element in selected color
function colorize(elem) {

  seed = string_to_seed(elem.innerHTML)
  
  sum = 400;
  r = Math.floor(srand() * (200));
  g = Math.floor(srand() * (200));
  if ((sum - r - g) < 200) {
    b = sum - r - g;
  } else {
    b = Math.floor(srand() * (250));
  }
  
  //This is wrong
  elem.style = 'color:rgb(' + r + ', ' + g + ', ' + b + ')';
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};
