//Crutches and bisycles

//Takes seconds, returns readable values
//Probably I should adjust them to some format
function timeleft (time) {
  days = Math.floor(time / 86400)
  hours = Math.floor((time % 86400) / 3600)
  minutes = Math.floor(time % 3600 / 60)
  return '' + days + 'd' + hours + 'h' + minutes + 'm'
}

function makeTimeReadable (elem) {
  elem.innerHTML = timeleft(parseInt(elem.innerHTML))
}
