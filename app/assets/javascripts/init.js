//All sorts of initializers

function init() {
  elems = document.getElementsByClassName('public_name');
  for (i = 0; i < elems.length; ++i) {
      colorize(elems[i]);
  }
}

var ready;
ready = function() {
  users = document.getElementsByClassName('public_name');
  for (i = 0; i < users.length; ++i) {
      colorize(users[i]);
  }
  ips = document.getElementsByClassName('ip_digest');
  for (i = 0; i < ips.length; ++i) {
      colorize(ips[i]);
  }
};

$(document).ready(ready);
$(document).on('page:load', ready);
