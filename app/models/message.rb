# == Schema Information
#
# Table name: messages
#
#  id          :integer          not null, primary key
#  content     :text             default("")
#  ip_digest   :string(255)
#  public_name :string(255)      default("Porcupine")
#  topic_id    :integer
#  board_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Message < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include ApplicationHelper
  validates_acceptance_of :sticky
  belongs_to :topic
  belongs_to :board #this is excessive, actually
  has_many :uploads
  accepts_nested_attributes_for :uploads
  validates_associated :uploads
#  validates :content, length: { minimum: 1 } #should be valid with images
  validate :must_have_content
  before_create :create_topic
  before_create :process_content
  after_create :set_public_name
  before_destroy :destroy_topic
  apply_simple_captcha

#  def assign_ip
#    self.ip_digest = Digest::SHA1.hexdigest(self.ip)
#  end
  #Creates new topic if not assigned to other one
  def create_topic
    if topic_id.nil?
      @board = self.board
      @topic = @board.topics.create!()
      self.topic_id = @topic.id
    else
      @topic = self.topic
      self.board_id = @topic.board_id
      @topic.updated_at = Time.now
      @topic.save
    end
  end
  
  def process_content
    #Applies markup
    self.processed_content = markup(self.content)
  end
  
  def set_public_name
    if self.ip_digest# && self.topic_id
      #Same user in same topic has same public name
      srand(self.ip_digest.gsub(/[a-zA-Z]/,'').to_i + self.topic_id)
      #Or some other generator:
      self.update_attribute(:public_name, Faker::Name.name)
    else
      #Just for tests:
      self.public_name = "Porcupine"
    end
  end
  
  def ip=(raw_ip)
    self.ip_digest = create_ip_digest(raw_ip)
  end
  
  def destroy_topic
    #If opening post is destroyed, replies are destroyed too, as well as topic
    if self.op?
      self.topic.messages[1..-1].each do |x|
        x.destroy
      end
      self.topic.destroy
    end
  end
  
  def op?
    self.topic.messages[0] == self
  end
  
  def link
    if !self.board.nil? && !self.topic.nil?
      board_topic_path(self.topic.board.address, self.topic.id) + "##{self.id}"
    else
      "IM ORPHAN :'-("
    end
  end

  def must_have_content
    errors.add(:content, (I18n.t :post_is_empty)) if (uploads.empty? && content.empty?)
    errors.add(:content, (I18n.t :post_is_too_big)) if (content && content.length > 18002)
  end
  #Retrieves link leading straight to this message
  def path
    board_topic_path(self.topic.board, self.topic) + "#" + self.id.to_s
  end
end
