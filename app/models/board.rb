# == Schema Information
#
# Table name: boards
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  address     :string(255)
#  description :string(255)      default("")
#  created_at  :datetime
#  updated_at  :datetime
#

class Board < ActiveRecord::Base
  has_many :user_board_relationships, class_name:  "UserBoardRelationship",
                                      foreign_key: "board_id",
                                      dependent:   :destroy
  has_many :users, through: :user_board_relationships, source: :user
  has_many :topics, :dependent => :destroy
  has_many :messages, :dependent => :destroy
  has_many :ip_statuses
  #after_create :init_assign_owner
  apply_simple_captcha
  validates :title, presence: true, length: { minimum: 3, maximum: 50 }, uniqueness: { case_sensitive: false }
  validates :description, length: { maximum: 220 }
  VALID_ADDRESS_REGEXP = /\A[a-zA-Z]\w+\z/i
  validates :address, presence: true, 
                      length: { maximum: 20 },
                      format: { with: VALID_ADDRESS_REGEXP },
                      uniqueness: { case_sensitive: true }
  before_save :downcase_stuff

=begin
  def init_assign_owner
    ##If Board is created from User, make him owner
    #if self.users.any? 
      to_change = self.user_board_relationships.first
      to_change.role = "owner"
      to_change.save!
    #end
  end
=end

  def assign_user(user, role="mod")
    if user.nil?
      self.errors.add(:user_board_relationships, "There is no user with such id")
    else
      ubr = self.user_board_relationships.create!(user_id: user.id, role: role)
    end
  end

  def to_param
    address.parameterize
  end 

  def owner
    ubr = self.user_board_relationships.where(role: 'owner').first
    if !ubr.nil?
      return User.find(ubr.user_id)
    else
      return nil
    end
    #This opens undesirable possibility of having multiple owners
  end

  private
  
    def downcase_stuff
      self.address = address.downcase
    end 
end
