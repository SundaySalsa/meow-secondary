# == Schema Information
#
# Table name: user_board_relationships
#
#  id         :integer          not null, primary key
#  role       :string(255)
#  user_id    :integer
#  board_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserBoardRelationship < ActiveRecord::Base
  #UBRs created without specified role are assigned to "owner"
  #Be careful around that
  belongs_to :user, class_name: "User"
  belongs_to :board, class_name: "Board"
  validates :user_id, presence: true
  validates :board_id, presence: true
  after_create :init_owner

  def init_owner
    #Not very safe, but I don't know how to make it other way
    if self.role.nil? or self.role.empty?
      self.role = 'owner'
      self.save
    end
  end
end
