# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  sticky     :boolean          default(FALSE)
#  open       :boolean          default(TRUE)
#  board_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Topic < ActiveRecord::Base
  belongs_to :board
  #NOT DEPENDENT DESTROY, SEE message.rb#destroy_topic
  has_many :messages, -> { order('updated_at ASC') } 
  default_scope -> { order('sticky DESC, updated_at DESC') }

  def op
    self.messages[0]
  end

  def latest_replies
    #self.messages.count < 2 ? [] : self.messages[-3..-1]
  #This chinese code is ugly, but it doesn't call database twice:
begin
    if self.messages.count < 2
      []
    elsif self.messages.count == 2
      [self.messages[-1]]
    elsif self.messages.count == 3
      self.messages[-2..-1]
    else
      self.messages[-3..-1]
    end
end
  end
end
