# == Schema Information
#
# Table name: uploads
#
#  id                :integer          not null, primary key
#  type              :string(255)
#  message_id        :integer
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  file_fingerprint  :string(255)
#  board_id          :integer
#

class Upload < ActiveRecord::Base
  belongs_to :message
  has_attached_file :file, :styles => { :thumb => "150x150>" }, :default_url => "/images/missing.png",
      :url => "/crap/:style/:basename.:extension", dependent: :destroy
  validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  before_create :randomize_file_name, :set_board
  validate :server_has_enough_space?
  #validates_uniqueness_of :file_fingerprint, :message => "Такой файл уже загружен" #uniqueness in board scope
  
  def server_has_enough_space? 
#    if !enough_space?
#      errors.add(:upload_file_size, "server doesn't have enough space to save this file")
#    end 
  end

  def set_board
    #Needed for message uniquiness verification
    self.attributes = { :board_id => self.message.board_id }
#    self.board_id = self.message.board_id if !self.message.nil?
#    update_attribute(:board_id, self.message.board_id) if !self.message.nil?
  end
  private
    def randomize_file_name
      self.file.instance_write(:file_name, "meowta_#{SecureRandom.hex(16)}#{File.extname(file_file_name).downcase}") if self.file_file_name
    end   
end
