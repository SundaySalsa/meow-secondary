# == Schema Information
#
# Table name: ip_statuses
#
#  id         :integer          not null, primary key
#  ip_digest  :string(255)
#  board_id   :integer
#  status     :string(255)
#  reason     :string(255)
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#

class IpStatus < ActiveRecord::Base
  require 'resolv'
  belongs_to :board
  belongs_to :user
  validates_acceptance_of :days_left, :board_address
  validates :ip_digest, :presence => true, length: { minimum: 7 }
  before_create :default_expiration_time
  before_create :default_values

  def default_values
    self.status ||= 'ban'
    self.reason = 'Из вредности' if self.reason.nil? || self.reason.empty?
    self.expires_at ||= 30.years.from_now
  end

  def default_expiration_time
    if self.expires_at.nil?
      self.expires_at = 30.years.from_now
    end
  end

  def days_left=(num)
    self.expires_at = num.to_f.days.from_now
  end

  def ip=(ip)
    if ip =~ Resolv::IPv4::Regex 
      self.ip_digest = Digest::SHA1.hexdigest(ip)
    else
      self.errors.add(:ip, "It isn't legit ip")
    end
  end
end
