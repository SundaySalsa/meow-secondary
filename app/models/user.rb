# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  email             :string(255)
#  password_digest   :string(255)
#  remember_token    :string(255)
#  admin             :boolean          default(FALSE)
#  about             :text
#  created_at        :datetime
#  updated_at        :datetime
#  remember_digest   :string(255)
#  activation_digest :string(255)
#  activated         :boolean          default(FALSE)
#  activated_at      :datetime
#

class User < ActiveRecord::Base
  include ApplicationHelper

  has_many :user_board_relationships, class_name:  "UserBoardRelationship",
                                      foreign_key: "user_id",
                                      dependent:   :destroy
  has_many :boards, through: :user_board_relationships, source: :board
  has_many :ip_statuses #aka bans and warnings

  attr_accessor :remember_token, :activation_token
  before_create :create_activation_digest
  before_save { self.email = email.downcase }
  VALID_NAME_REGEX = /\A[a-z .0-9_-]{3,25}/i
  validates :name, presence: true,
                   format:     { with: VALID_NAME_REGEX },
                   uniqueness: { case_sensitive: false }
  validates :about, length: { maximum: 6000 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: { minimum: 4 }, allow_blank: true
  validate :user_amount_is_limited
  #apply_simple_captcha

#  def User.create_board
#    self.boards.create!
#  end
  
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

 def user_amount_is_limited
    errors.add(:id, "There are too many users already") if User.count > max_users
  end

  private
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end
