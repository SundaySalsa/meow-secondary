module BoardsHelper
  def is_in_board?
    !params[:address].nil? or !params[:board_address].nil?
  end

  def board_sym
    if params[:address]
      :address
    elsif params[:board_address]
      :board_address
    else
      nil
    end
  end
  
  def ban
    ip = create_ip_digest(request.remote_ip)
    if board_sym
      @board = Board.find_by_address(board_sym)
      IpStatus.where('expires_at > ?', Time.now).find_by_ip_digest(ip)
    end
  end
end
