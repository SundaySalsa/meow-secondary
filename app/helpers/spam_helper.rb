module SpamHelper
  def spam_images(topic, images_folder)
    (rand(10) > 1) ? files_count = 6 : files_count = 1 #Randomizing it a little bit to look less suspicious
    names = Dir.glob(images_folder + '/*').select{ |e| File.file? e }[0..(files_count-1)]
    @topic = Topic.find(topic)
    if names.count > 0
      names.map!{ |el| Rails.root.join(el)} #full names
      uploads = names.map{ |el| Upload.new(file: File.open(el))}    
      @topic.messages.create!(board: @topic.board, uploads: uploads, ip: '127.0.0.1')
    #cleanup
      names.each { |path_to_file| File.delete(path_to_file) if File.exist?(path_to_file)}
    else
      #do nothing
    end    
  end

  def self.ugly_spam_images
    #Even weirder workaround because I haven't figured out how to pass arguments through schedule.rb
    #Threads and image paths are basically hardcoded here
    #hash = {67 => "public/crap/spam/bloody_maryam"}
    #hash = Hash.new #no threads yet
    hash = {22 => 'public/crap/spam/bloody_maryam'}
    topic_id = hash.keys.sample
    images_path = hash[topic_id]
#    hash.each do |topic_id, images_path|
      (rand(10) > 1) ? files_count = 6 : files_count = 1 #Randomizing it a little bit to look less suspicious
      names = Dir.glob(images_path + '/*').select{ |e| File.file? e }[0..(files_count-1)]
      @topic = Topic.find(topic_id)
      if names.count > 0
        names.map!{ |el| Rails.root.join(el)} #full names
        uploads = names.map{ |el| Upload.new(file: File.open(el))}    
        @topic.messages.create!(board: @topic.board, uploads: uploads, ip: '127.0.0.1')
        #cleanup
        names.each { |path_to_file| File.delete(path_to_file) if File.exist?(path_to_file)}
      else
        #do nothing
      end 
#    end
  end
  
=begin
  def self.spam_images_runner
    #Weird workaround needed for crontab tasks
    #Spamhelper.spam_images_runner 67 'public/crap/spam/something'
    #ARGV[0] is topic id
    #ARGV[1] is folder with images
    (rand(10) > 1) ? files_count = 6 : files_count = 1 #Randomizing it a little bit to look less suspicious
    names = Dir.glob(ARGV[1] + '/*').select{ |e| File.file? e }[0..(files_count-1)]
    @topic = Topic.find(ARGV[0])
    if names.count > 0
      names.map!{ |el| Rails.root.join(el)} #full names
      uploads = names.map{ |el| Upload.new(file: File.open(el))}    
      @topic.messages.create!(board: @topic.board, uploads: uploads, ip: '127.0.0.1')
    #cleanup
      names.each { |path_to_file| File.delete(path_to_file) if File.exist?(path_to_file)}
    else
      #do nothing
    end 
  end
=end
end
