module ApplicationHelper
  def broadcast(channel, &block)
    #This helper is used for interaction with faye server
    #i. e. thing that is used for message autoloading, instead of polling
    message = {:channel => channel, :data => capture(&block)}
    uri = URI.parse("http://localhost:9292/faye")
    Net::HTTP.post_form(uri, :message => message.to_json)
  end
  
  def dhms (seconds)
    return ([60,60,24].reduce([seconds]) { |m,o| m.unshift(m.shift.to_i.divmod(o)).flatten })
  end

  def markdown(text)
    raw(markup(text))
  end

  def markup(text)
    #Results should EXACTLY match hardcoded tests
    #See spec/helpers/application_helper_spec.rb for desired results
    #I'm sorry for that --Alpha

    result = text.dup
    result.gsub!(/(<|&#60;)/, '&lt;')
    result.gsub!(/(>|&#62;)/, '&gt;')

    #I have no idea how to make this more abstract
    bold_regex = /\[b\][\S\s]*?\[\/b\]/i
    result.gsub!(bold_regex) {|k| "<b>#{k[3..-5]}</b>"}
    italic_regex = /\[i\][\S\s]*?\[\/i\]/i
    result.gsub!(italic_regex) {|k| "<em>#{k[3..-5]}</em>"}
    result.gsub!(/\[s\][\S\s]*?\[\/s\]/i) {|k| "<s>#{k[3..-5]}</s>"}
    result.gsub!(/\[u\][\S\s]*?\[\/u\]/i) {|k| "<u>#{k[3..-5]}</u>"}
    result.gsub!(/\[spoiler\][\S\s]*?\[\/spoiler\]/i) {|k| "<span class='spoiler'>#{k[9..-11]}</span>"}

    #A bit of wakaba
    
    result.gsub!(/^.+\n\*\*\*\*\*\*\*\*\*\*/) {|k| "<h3>#{k[0..-11]}</h3>"}
    #Those are single-liners. I'm not even sure we actually need those
    result.gsub!(/\*\*.*?\*\*/i) {|k| "<b>#{k[2..-3]}</b>"}
    #order is important!    
    result.gsub!(/\*.*?\*/i) {|k| "<em>#{k[1..-2]}</em>"}
    result.gsub!(/\%\%.*?\%\%/i)  {|k| "<span class='spoiler'>#{k[2..-3]}</span>"}

    #Paragraphs
    result.gsub!(/\n/, '<br/>')
    
    #Quote links
    #>>111 => /b/23#111
    def link_to_message(num)
      if Message.exists? id: num        
        @msg = Message.find(num)
        ActionController::Base.helpers.link_to ">>#{num}", @msg.link
      else
        "&gt;&gt;#{num}"
      end
    end
    result.gsub!(/&gt;&gt;\d+/) {|k| link_to_message(k[8..-1].to_i) }

    #URLs
    valid_url = /(https?|ftp):\/\/[^\s\/$.?#].[^\s]*/i
    result.gsub!(valid_url) { |k| '<a rel="nofollow" href="' + k + '">' + k + '</a>'}

    return result
  end

  def full_title(txt=nil)
    # if an argument is symbol, it returns i18n-translation
    # Otherwise, it only returns 
    txt ? ("#{ t :meowta } - " + txt) : (t :meowta)
  end

  # Calculates size of target folder in file system
  def folder_size(name)
    if File.directory?(name)
      (Dir.entries(name) - ['.','..']).inject(0) { |sum,file| sum +
       folder_size(File.join(name,file)) }
    else
      File.size(name)
    end
  end

  def enough_space?
    max_size = 1073741824  # 1 Gb. Change this after moving to better server
    folder_size(Rails.root.join('public')) < max_size
  end

  def max_users
    500
  end

  def restricted_addresses
    ["admin", "banmsg", "ban", "bans", "board", "boards", "create", "faq", "manifesto", "messages", "not_found", "reply",
     "root", "rules", "session", "sessions", "signin", "signout", "signup", "unban", "user", "users", "welcome"]
  end

  def create_ip_digest(ip)
    token = Meow::Application.config.secret_token
    token = 'foo' if token.nil? 
    Digest::SHA1.hexdigest(ip + token)
  end
end
