class UserBoardRelationshipsController < ApplicationController
  def create
    store_location
    @board = Board.find_by_address(params[:board_address])
    @user = User.find_by_name(params[:name])
    @users = @board.users.all.paginate(page: params[:page])
    #role params?
    if @user.nil? or !admin_or_board_owner?(@board)
      flash[:error] = 'There is no such user'
      redirect_to board_users_path(@board)
    else
      @board.assign_user(@user, "mod")
      flash[:success] = 'User assigned'
      redirect_to board_users_path(@board)
    end
  end
  
  def destroy
    @board = Board.find_by_address(params[:board_address])
    @user = User.find(params[:id])
    @ubr = @board.user_board_relationships.where(user_id: @user.id)[0]
    if @ubr.destroy!
      flash[:success] = '...'
    else
      flash[:error] = '...'
    end
    redirect_to board_users_path(@board)
  end
end
