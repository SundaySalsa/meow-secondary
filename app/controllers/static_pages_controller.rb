class StaticPagesController < ApplicationController
  include Rails.application.routes.url_helpers

  def home
    Upload.all.count > 1 ? @pics = Upload.all.shuffle[0..4] : @pics = []
    Topic.all.count > 1 ? @topics = Topic.all.shuffle[0..4] : @topics = []
  end

  def faq
    add_breadcrumb I18n.t('faq'), 'faq'
    @examples = ["*Bold*", "[b]Bold[/b]", "**Italic**", "[i]Italic[/i]",
     '[s]Strike[/s]', '[u]Underscore[/u]', '%%Spoiler%%', "[spoiler]Spoiler[/spoiler]", 
     'http://meowta.tk', '>>1']
  end
  
  def rules
    add_breadcrumb I18n.t('rules'), 'rules'
  end
  
  def error_404
    add_breadcrumb '?????', '/'
  end
  
  def error_500
  end
  
end
