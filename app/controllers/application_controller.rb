class ApplicationController < ActionController::Base
  include SimpleCaptcha::ControllerHelpers
  include SessionsHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

#  add_breadcrumb "meowta", :root_path, :options => { :title => I18n.t "meowta" }

  def create_ip_digest(ip)
    #Move this into secret git-ignored file
    Digest::SHA1.hexdigest(ip + "foo")
  end
end
