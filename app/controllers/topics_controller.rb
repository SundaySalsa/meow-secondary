class TopicsController < ApplicationController

  def index
    @topics = Topic.where('id > ?', params[:after_id].to_i).order('created_at DESC')
  end

  def show
    @topic ||= Topic.find(params[:id])
    @board ||= @topic.board
    @replies = @topic.messages[1..-1] or []
    add_breadcrumb I18n.t('boards'), boards_path
    add_breadcrumb @topic.board.title, board_path(@topic.board.address)
    add_breadcrumb @topic.id, @topic.id.to_s #???
    @reply ||= @topic.messages.new
    6.times { @reply.uploads.build }
  end
end
