class UsersController < ApplicationController
  include SimpleCaptcha::ControllerHelpers
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  def create
    @user = User.new(user_params)
    if @user.valid? && simple_captcha_valid?
      #I'm using https://github.com/pludoni/simple-captcha
      #They have prettier forms, but they don't work
      #:(
      @user.save
      log_in @user
      redirect_to @user
    else
      #flash captcha error
      render 'new'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def edit
    @user = User.find(params[:id])
  end

  def index
    add_breadcrumb I18n.t('users'), 'index'
    @users = User.paginate(page: params[:page])
  end

  def new
    add_breadcrumb I18n.t('new_user'), 'signup'
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    add_breadcrumb @user.name, params[:id]
    @boards = @user.boards.paginate(page: params[:page]).uniq
#    debugger
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Pröfile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:user).permit(:about, :name, :email, :password,
                                   :password_confirmation)
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please lög in."
        redirect_to login_url
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
