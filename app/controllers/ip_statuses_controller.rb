class IpStatusesController < ApplicationController
include ApplicationHelper

  def create
    @ip_status = IpStatus.new(ip_status_params)
    #This is probably wrong
    @ip_status.board_id = Board.find_by_address(params[:ip_status][:board_address]).id
    @ip_status.user ||= current_user if logged_in?
    if @ip_status.save
      flash[:success] = "Done!"
      redirect_to board_mod_path(@ip_status.board)
    else
      flash[:error] = @ip_status.errors.full_messages.join(', ')
      redirect_to board_mod_path(@ip_status.board)
    end
  end

  def destroy
    #fds
    @board = Board.find_by_address(params[:board_address])
    @ip_status = IpStatus.find(params[:ip_status][:id])
    @time = Time.now
    if mod?(@board) && @time < @ip_status.expires_at
      @ip_status.update_attribute(:expires_at, @time) #tee-hee
      flash[:success] = "Done!"
      redirect_to board_mod_path(@board)
    else
      flash[:error] = 'Something went wrong'
      redirect_to board_mod_path(@board)
    end
  end

  def index
  end

  private
    def ip_status_params
      params.require(:ip_status).permit(:reason, :days_left, :ip_digest)
    end

end
