class MessagesController < ApplicationController
include SessionsHelper
include ApplicationHelper
#before_action :admin_user, only: [:destroy]
before_action :not_banned, only: [:create]

  def create
    @board = Board.find_by_address(params[:board_address])
    @topic = Topic.find(params[:message][:topic_id]) if params[:message][:topic_id]
    #If topic isn't found, build message from board, else build it from found topic
    if @topic.nil? 
      @msg = @board.messages.new(message_params)
    else
      @msg = @topic.messages.new(message_params)
    end
    @msg.ip_digest = create_ip_digest(request.remote_ip)
    store_location
    if @msg.valid_with_captcha?
      flash[:success] = "..."
      @msg.save
      redirect_to board_topic_path(@board, @msg.topic) #change to rendering
    else
      flash[:error] = "Something went wrong"
      if !@topic.nil?
        @replies = @topic.messages.all
        @reply = @msg
        (6-params[:message][:uploads_attributes].count).times { @message.uploads.build } if params[:message][:uploads_attributes]
        render 'topics/show'
      else
        @topics = @board.topics.all.paginate(page: params[:page])
        @message = @msg
        (6-params[:message][:uploads_attributes].count).times { @message.uploads.build } if params[:message][:uploads_attributes]
        render 'boards/show'
      end
    end
  end

=begin
  def create_reply
    #Creates reply, i. e. Message from Topic and assigns it to it
    #It's a bit messy, see models/Message.rb for more details
    @topic = Topic.find(params[:message][:topic_id])
    @reply = @topic.messages.create(message_params)
    @reply.ip_digest = create_ip_digest(request.remote_ip)
    if @reply.save
      redirect_to board_topic_path(@topic.board, @topic) #change to rendering
    else
      flash[:error] = "Something went wrong"
      (5 - @reply.uploads.count).times { @reply.uploads.build }
      render 'reply'
    end
  end

  def create_topic
    #Creates new thread, i. e. Topic and assigns new message as its first Message
    @board = Board.find_by_id(params[:message][:board_id])
    @message = @board.messages.new(message_params)
    @message.ip_digest = create_ip_digest(request.remote_ip)
    if @message.save
      if admin_or_board_owner?(@board)
        @topic = @message.topic
        @topic.sticky = true if params[:message][:sticky] == "1"
        @topic.open = false if params[:message][:open] == "0" #default is true
        @topic.save
      end
      redirect_to board_topic_path(@board, @message.topic)
    else
      #re-render field, leave board intact
      #needs ajax
      #or at least render new page
      flash[:error] = "Something went wrong"
      render 'new'
    end
  end
=end

  def destroy
    @msg = Message.find(params[:id])
    @board = @msg.board   
    if current_user && @board.messages.include?(@msg) && (@board.users.include?(current_user) || current_user.admin?)
      if @msg.delete
        flash[:success] = "Message deleted"
      else
        flash[:error] = t :message_delete_error
      end
      redirect_to board_mod_path(@board)
    else
      redirect_to root_path
    end
  end

  private

    def message_params
      params.require(:message).permit(:content, :captcha, :topic_id, :captcha_key, uploads_attributes: [:file])
    end
    
    def admin_user
      redirect_to root_path unless current_user && current_user.admin?
    end
    
    def not_banned
      store_location
      @board = Board.find_by_address(params[:board_address])
      @ip_digest = create_ip_digest(request.remote_ip)
      if @board.ip_statuses.find_by_ip_digest(@ip_digest)
        flash[:error] = "U r b&"
        redirect_back_or(root_url) 
      end
    end
end
