class BoardsController < ApplicationController
before_action :logged_in_user, only: [:create, :new]
before_action :admin_user, only: [:destroy]

  def create
    @board = current_user.boards.new(board_params)
    @user = current_user
    if @board.valid?
      @board.save
      UserBoardRelationship.create!(board_id: @board.id, user_id: @user.id, role: 'owner')
      redirect_to @board
    else
      render 'new'
      flash[:error] = "captcha"
    end
  end

  def destroy
    @board = Board.find_by_address(params[:board_address])
    @board.delete
    flash[:success] = "Board destroyed"
    redirect_to boards_path
  end

  def edit
    @board = Board.find_by_address(params[:board_address])
  end

  def index
    add_breadcrumb I18n.t('boards'), "boards"
    @boards = Board.paginate(page: params[:page])
  end
  
  def mod
    @board = Board.find_by_address(params[:board_address])
    add_breadcrumb @board.title, @board
    add_breadcrumb 'mod', 'mod', class: 'active'
    if mod?(@board)
      #render forms
      @ip_status = IpStatus.new
      @ip_statuses = @board.ip_statuses
    else 
      redirect_to root_url
    end
  end

  def new
    add_breadcrumb I18n.t('new_board'), 'new'
    @board = Board.new
  end

  def show
    @board = Board.find_by_address(params[:address])
    add_breadcrumb I18n.t('boards'), boards_path
    add_breadcrumb @board.title, @board.address, class: 'active'
    @topics = @board.topics.all.paginate(page: params[:page])
    @message = Message.new             #Message form
    6.times { @message.uploads.build }
  end

  def update
    @board = Board.find_by_address(params[:board_address])  
    if @board.update_attributes(board_edit_params)
      flash[:success] = "Сохранено"
      redirect_to @board
    else
      render 'edit'
    end
  end
  
  def users
    @board = Board.find_by_address(params[:board_address])  
    add_breadcrumb I18n.t('boards'), boards_path
    add_breadcrumb @board.title, @board.address
    add_breadcrumb I18n.t('users'), 'users', class: 'active'
    @users = @board.users.all.paginate(page: params[:page])
  end

  private

    def admin_user
      redirect_to root_path unless current_user && current_user.admin?
    end

    def board_params
      params.require(:board).permit(:title, :address, :description)
    end

    def board_edit_params
      params.require(:board).permit(:title, :description)    
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please lög in."
        redirect_to login_url
      end
    end
end
